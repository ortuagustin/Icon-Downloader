object FGUIMain: TFGUIMain
  Left = 0
  Top = 0
  Caption = 'Icon Downloader'
  ClientHeight = 349
  ClientWidth = 637
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object gpBottomToolBar: TGridPanel
    Left = 0
    Top = 249
    Width = 637
    Height = 100
    Align = alBottom
    Caption = 'gpBottomToolBar'
    ColumnCollection = <
      item
        Value = 79.069767441860450000
      end
      item
        Value = 20.930232558139550000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = edDestinationFolder
        Row = 1
      end
      item
        Column = 1
        Control = BtnDownload
        Row = 2
      end
      item
        Column = 1
        Control = BtnChangeDestFolder
        Row = 1
      end
      item
        Column = 0
        Control = Label2
        Row = 0
      end>
    RowCollection = <
      item
        Value = 29.987830099228490000
      end
      item
        Value = 30.012169900771550000
      end
      item
        Value = 39.999999999999960000
      end>
    ShowCaption = False
    TabOrder = 2
    DesignSize = (
      637
      100)
    object edDestinationFolder: TEdit
      AlignWithMargins = True
      Left = 4
      Top = 32
      Width = 489
      Height = 22
      Margins.Top = 2
      Margins.Right = 10
      Margins.Bottom = 5
      Align = alClient
      TabOrder = 0
      ExplicitHeight = 21
    end
    object BtnDownload: TButton
      AlignWithMargins = True
      Left = 506
      Top = 62
      Width = 127
      Height = 34
      Align = alClient
      Caption = 'Download'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = BtnDownloadClick
    end
    object BtnChangeDestFolder: TButton
      Left = 553
      Top = 32
      Width = 33
      Height = 24
      Anchors = []
      Caption = '...'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = BtnChangeDestFolderClick
    end
    object Label2: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 127
      Height = 23
      Align = alLeft
      Caption = 'Directorio destino:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlCenter
      ExplicitHeight = 19
    end
  end
  object pTopToolBar: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 631
    Height = 42
    Align = alTop
    Caption = 'pTopToolBar'
    ShowCaption = False
    TabOrder = 0
    object Label1: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 30
      Height = 34
      Align = alLeft
      Caption = 'URL:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlCenter
      ExplicitHeight = 19
    end
    object cbURLS: TComboBox
      AlignWithMargins = True
      Left = 40
      Top = 4
      Width = 587
      Height = 27
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
  object Memo1: TMemo
    Left = 0
    Top = 48
    Width = 637
    Height = 201
    Align = alClient
    TabOrder = 1
  end
end
