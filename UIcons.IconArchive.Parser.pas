unit UIcons.IconArchive.Parser;

interface

uses
  UIcons.BaseParser, UIcons.Types;

const
  IA_BASE_URL = 'icons.iconarchive.com';
  IA_DELIM = '/';

type
  { NOTE: This Parser should always set the first item in the return list to be the one matching the size
    on the requested url. This is beceause if requested download mode is dmUrlOnly }
  TIconArchiveUrlParser = class(TInterfacedObject, IUrlParser, IIsBaseUrl)
  public
    function IsBaseUrl(AUrl: string): Boolean;
    function ParseUrl(const AUrl: string): TParsedImageList;
  end;

implementation

uses
  System.SysUtils, System.Classes;

const
  SUPPORTED_SIZES: TImageSizes = [iz16 .. iz128];

{ TIconArchiveUrlParser }

function TIconArchiveUrlParser.IsBaseUrl(AUrl: string): Boolean;
begin
  if AUrl.StartsWith(HTTP, True) then
    AUrl := AUrl.Replace(HTTP, EmptyStr, [rfReplaceAll, rfIgnoreCase]);

  Result := AUrl.StartsWith(IA_BASE_URL, True);
end;

function TIconArchiveUrlParser.ParseUrl(const AUrl: string): TParsedImageList;
var
  AString, FormattableString: string;
  UrlImageSize: Integer;
  AImageSize: TImageSize;
  LItem: TParsedImage;
  UrlComponents: TStringList;
begin
  if AUrl = EmptyStr then
    raise Exception.Create('Url vac�a!');

  if not(IsBaseUrl(AUrl)) then
    raise Exception.Create('Url no es valida para ' + IA_BASE_URL);

  UrlComponents := TStringList.Create;
  try
    UrlComponents.Delimiter := IA_DELIM;
    UrlComponents.StrictDelimiter := True;
    UrlComponents.DelimitedText := AUrl;

    for AString in UrlComponents do
    begin
      if TryStrToInt(AString, UrlImageSize) then
      begin
        FormattableString := FormattableString + '%d';
      end else
        FormattableString := FormattableString + AString;
      FormattableString := FormattableString + IA_DELIM;
    end;
    SetLength(FormattableString, FormattableString.Length - 1);

    Result := TParsedImageList.Create;
    for AImageSize in SUPPORTED_SIZES do
    begin
      LItem := TParsedImage.Create;

      if AUrl.EndsWith(EXT_PNG) then
        LItem.ImageKind := ikPng
      else if AUrl.EndsWith(EXT_ICO) then
        LItem.ImageKind := ikIco;

      LItem.FullUrl := Format(FormattableString, [AImageSize.ToInteger]);
      LItem.FileName := UrlComponents.Strings[UrlComponents.Count - 1];
      LItem.ImageSize := AImageSize;

      Result.Add(LItem);
    end;
  finally
    UrlComponents.Free;
  end;
end;

end.
