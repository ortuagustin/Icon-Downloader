unit UIcons.Main;

interface

uses
  UIcons.Types, UIcons.BaseParser, System.Classes;

type
  IImageDownload = interface
    ['{B9089147-2756-4E45-83D3-4F4C9B547BB4}']
    procedure SetMode(const Value: TDownloadMode);
    procedure SetOnCompleted(const Value: TOnCompletedEvent);
    procedure SetOverWrite(const Value: Boolean);
    function GetMode: TDownloadMode;
    function GetOnCompleted: TOnCompletedEvent;
    function GetOverwrite: Boolean;

    procedure Download(const ADest: string);
    property DownloadMode: TDownloadMode read GetMode write SetMode;
    property Overwrite: Boolean read GetOverwrite write SetOverWrite;
    property OnCompleted: TOnCompletedEvent read GetOnCompleted write SetOnCompleted;
  end;

  TCustomIconDownloaderClass = class of TCustomIconDownloader;

  TCustomIconDownloader = class abstract(TInterfacedObject, IImageDownload)
  private
    FMode: TDownloadMode;
    FOnCompleted: TOnCompletedEvent;
    FUrl: string;
    FOverwrite: Boolean;
    procedure SetUrl(const Value: string);
    procedure SetMode(const Value: TDownloadMode);
    procedure SetOnCompleted(const Value: TOnCompletedEvent);
    procedure SetOverWrite(const Value: Boolean);
    function GetMode: TDownloadMode;
    function GetOverwrite: Boolean;
    function GetOnCompleted: TOnCompletedEvent;
  protected
    class function GetClass(const AUrl: string): TCustomIconDownloaderClass;
    class procedure Register(AClass: TCustomIconDownloaderClass; AInterfaceList: TInterfaceList);
    constructor Create(const AUrl: string); virtual;
    function BuildDownloadedItems(ParsedImages: TParsedImageList): TImageInfoList; virtual; abstract;
  public
    class function New(const AUrl: string): TCustomIconDownloader;
    procedure Download(const ADest: string); virtual; abstract;
    property Url: string read FUrl write SetUrl;
    property DownloadMode: TDownloadMode read GetMode write SetMode;
    property OnCompleted: TOnCompletedEvent read GetOnCompleted write SetOnCompleted;
    property Overwrite: Boolean read GetOverwrite write SetOverWrite default False;
  end;

implementation

uses
  UIcons.Factory;

{ TCustomIconDownloader }

class function TCustomIconDownloader.New(const AUrl: string): TCustomIconDownloader;
var
  AClass: TCustomIconDownloaderClass;
begin
  AClass := GetClass(AUrl);
  Result := AClass.Create(AUrl);
end;

constructor TCustomIconDownloader.Create(const AUrl: string);
begin
  inherited Create;
  FUrl := AUrl;
end;

class function TCustomIconDownloader.GetClass(const AUrl: string): TCustomIconDownloaderClass;
begin
  Result := ImageFactory.GetClass(AUrl);
end;

class procedure TCustomIconDownloader.Register(AClass: TCustomIconDownloaderClass; AInterfaceList: TInterfaceList);
begin
  ImageFactory.RegisterClass(AClass, AInterfaceList);
end;

procedure TCustomIconDownloader.SetMode(const Value: TDownloadMode);
begin
  FMode := Value;
end;

procedure TCustomIconDownloader.SetOnCompleted(const Value: TOnCompletedEvent);
begin
  FOnCompleted := Value;
end;

procedure TCustomIconDownloader.SetOverWrite(const Value: Boolean);
begin
  FOverwrite := Value;
end;

procedure TCustomIconDownloader.SetUrl(const Value: string);
begin
  FUrl := Value;
end;

function TCustomIconDownloader.GetMode: TDownloadMode;
begin
  Result := FMode;
end;

function TCustomIconDownloader.GetOnCompleted: TOnCompletedEvent;
begin
  Result := FOnCompleted;
end;

function TCustomIconDownloader.GetOverwrite: Boolean;
begin
  Result := FOverwrite;
end;

end.
