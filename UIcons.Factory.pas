unit UIcons.Factory;

interface

uses
  UIcons.Main, System.Classes, System.Generics.Collections;

type
  TImgClassPair = TPair<TCustomIconDownloaderClass, TInterfaceList>;

  TImageFactory = class
  private
    FClassList: TDictionary<TCustomIconDownloaderClass, TInterfaceList>;
    constructor Create;
    function GetCount: Integer;
    function Satisfy(const AUrl: string; APair: TImgClassPair): Boolean;
  public
    destructor Destroy; override;
    function GetClass(const AUrl: string): TCustomIconDownloaderClass;
    procedure RegisterClass(AClass: TCustomIconDownloaderClass; AInterfaceList: TInterfaceList);
    property Count: Integer read GetCount;
  end;

var
  ImageFactory: TImageFactory;

implementation

uses
  UIcons.BaseParser, System.SysUtils;

{ TImageFactory }

constructor TImageFactory.Create;
begin
  inherited Create;
  FClassList := TDictionary<TCustomIconDownloaderClass, TInterfaceList>.Create;
end;

destructor TImageFactory.Destroy;
var
  LItem: TImgClassPair;
begin
  if Assigned(FClassList) then
  begin
    for LItem in FClassList do
      LItem.Value.Free;
    FClassList.Free;
  end;

  inherited Destroy;
end;

function TImageFactory.GetClass(const AUrl: string): TCustomIconDownloaderClass;
var
  LItem: TImgClassPair;
begin
  Result := NIL;
  try
    for LItem in FClassList do
    begin
      if Satisfy(AUrl, LItem) then
        Exit(LItem.Key);
    end;
  finally
    if Result = NIL then
      raise Exception.CreateFmt('TCustomIconDownloaderClass for %s not found', [AUrl]);
  end;
end;

function TImageFactory.GetCount: Integer;
begin
  Result := FClassList.Count;
end;

procedure TImageFactory.RegisterClass(AClass: TCustomIconDownloaderClass; AInterfaceList: TInterfaceList);
begin
  try
    if FClassList.ContainsKey(AClass) then
      raise Exception.CreateFmt('TCustomIconDownloaderClass %s already registered', [AClass.ClassName]);

  FClassList.Add(AClass, AInterfaceList);
  except
    AInterfaceList.Free;
  end;
end;

function TImageFactory.Satisfy(const AUrl: string; APair: TImgClassPair): Boolean;
var
  LItem: IInterface;
  AIsBaseUrl: IIsBaseUrl;
begin
  for LItem in APair.Value do
  begin
    if Supports(LItem, IIsBaseUrl, AIsBaseUrl) then
    begin
      if AIsBaseUrl.IsBaseUrl(AUrl) then
        Exit(True);
    end;
  end;

  Result := False;
end;

initialization
ImageFactory := TImageFactory.Create;

finalization
ImageFactory.Free;

end.
