unit UIcons.BaseParser;

interface

uses
  UIcons.Types, System.Generics.Collections;

type
  TParsedImage = class
  private
    FFileName: string;
    FFullUrl: string;
    FKind: TImageKind;
    FSize: TImageSize;
    FFilePath: string;
    FDownloadFolder: string;
    FFileSize: Int64;
    FOverwrited: Boolean;
    procedure SetFileName(const Value: string);
    procedure SetFullUrl(const Value: string);
    procedure SetKind(const Value: TImageKind);
    procedure SetSize(const Value: TImageSize);
    procedure SetFilePath(const Value: string);
    procedure SetDownloadFolder(const Value: string);
    procedure SetFileSize(const Value: Int64);
    procedure SetOverwrited(const Value: Boolean);
  public
    constructor Create;
    property FullUrl: string read FFullUrl write SetFullUrl;
    property FileName: string read FFileName write SetFileName;
    property FileSize: Int64 read FFileSize write SetFileSize;
    property DownloadFolder: string read FDownloadFolder write SetDownloadFolder;
    property FilePath: string read FFilePath write SetFilePath;
    property ImageSize: TImageSize read FSize write SetSize;
    property ImageKind: TImageKind read FKind write SetKind;
    property Overwrited: Boolean read FOverwrited write SetOverwrited;
  end;

  TParsedImageList = TObjectList<TParsedImage>;

  IUrlParser = interface
    ['{69451F8D-1170-4EC2-89E5-45DCA5042184}']
    function ParseUrl(const AUrl: string): TParsedImageList;
  end;

  IIsBaseUrl = interface
    ['{6DA62130-B440-4984-B27A-B1A0FF5D78AE}']
    function IsBaseUrl(AUrl: string): Boolean;
  end;

implementation

uses
  System.SysUtils;

{ TUrlData }

constructor TParsedImage.Create;
begin
  inherited Create;
  FFileName := EmptyStr;
  FFileSize := 0;
  FDownloadFolder := EmptyStr;
  FFilePath := EmptyStr;
  FFullUrl := EmptyStr;
  FKind := ikUnk;
  FSize := izUnk;
  FOverwrited := False;
end;

procedure TParsedImage.SetDownloadFolder(const Value: string);
begin
  FDownloadFolder := Value;
end;

procedure TParsedImage.SetFileName(const Value: string);
begin
  FFileName := Value;
end;

procedure TParsedImage.SetFilePath(const Value: string);
begin
  FFilePath := Value;
end;

procedure TParsedImage.SetFileSize(const Value: Int64);
begin
  FFileSize := Value;
end;

procedure TParsedImage.SetFullUrl(const Value: string);
begin
  FFullUrl := Value;
end;

procedure TParsedImage.SetKind(const Value: TImageKind);
begin
  FKind := Value;
end;

procedure TParsedImage.SetOverwrited(const Value: Boolean);
begin
  FOverwrited := Value;
end;

procedure TParsedImage.SetSize(const Value: TImageSize);
begin
  FSize := Value;
end;

end.
