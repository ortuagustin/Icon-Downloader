program IconDownloader;

uses
  Vcl.Forms,
  GUI.Main in 'GUI.Main.pas' {FGUIMain},
  UIcons.Factory in 'UIcons.Factory.pas',
  UIcons.IconArchive in 'UIcons.IconArchive.pas',
  UIcons.Types in 'UIcons.Types.pas',
  UIcons.IconArchive.Parser in 'UIcons.IconArchive.Parser.pas',
  UIcons.BaseParser in 'UIcons.BaseParser.pas',
  UIcons.Main in 'UIcons.Main.pas',
  UIcons.IconFinder in 'IconFinder\UIcons.IconFinder.pas',
  UIcons.IconFinder.Parser in 'IconFinder\UIcons.IconFinder.Parser.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFGUIMain, FGUIMain);
  ReportMemoryLeaksOnShutdown := True;
  Application.Run;
end.
