unit UIcons.IconFinder.Parser;

interface

uses
  UIcons.BaseParser, UIcons.Types;

const
  IF_BASE_URL = 'www.iconfinder.com/icons/';
  IF_DELIM = '/';

type
  { NOTE: This Parser should always set the first item in the return list to be the one matching the size
    on the requested url. This is beceause if requested download mode is dmUrlOnly }
  TIconFinderUrlParser = class(TInterfacedObject, IUrlParser, IIsBaseUrl)
  public
    function IsBaseUrl(AUrl: string): Boolean;
    function ParseUrl(const AUrl: string): TParsedImageList;
  end;

implementation

uses
  System.SysUtils, System.Classes;

const
  SUPPORTED_SIZES: TImageSizes = [iz16 .. iz128];

{ TIconFinderUrlParser }

function TIconFinderUrlParser.IsBaseUrl(AUrl: string): Boolean;
begin
  if AUrl.StartsWith(HTTP, True) then
    AUrl := AUrl.Replace(HTTP, EmptyStr, [rfReplaceAll, rfIgnoreCase]);

  if AUrl.StartsWith(HTTPS, True) then
    AUrl := AUrl.Replace(HTTPS, EmptyStr, [rfReplaceAll, rfIgnoreCase]);

  Result := AUrl.StartsWith(IF_BASE_URL, True);
end;

function TIconFinderUrlParser.ParseUrl(const AUrl: string): TParsedImageList;
var
  FormattableString: string;
  i, UrlImageSize: Integer;
  LImageKind: TImageKind;
  AImageSize: TImageSize;
  LItem: TParsedImage;
  UrlComponents: TStringList;
begin
  if AUrl = EmptyStr then
    raise Exception.Create('Url vac�a!');

  if not(IsBaseUrl(AUrl)) then
    raise Exception.Create('Url no es valida para ' + IF_BASE_URL);

  UrlComponents := TStringList.Create;
  try
    UrlComponents.Delimiter := IF_DELIM;
    UrlComponents.StrictDelimiter := True;
    UrlComponents.DelimitedText := AUrl;

    for i := 0 to UrlComponents.Count - 2 do
      FormattableString := FormattableString + UrlComponents[i] + IF_DELIM;

    if FormattableString.EndsWith('png' + IF_DELIM, True) then
      LItem.ImageKind := ikPng
    else if FormattableString.EndsWith('ico' + IF_DELIM, True) then
      LItem.ImageKind := ikIco
    else
      raise Exception.Create('Unknown image type');

    FormattableString := FormattableString + IF_DELIM + '%d';

    Result := TParsedImageList.Create;
    for AImageSize in SUPPORTED_SIZES do
    begin
      LItem := TParsedImage.Create;
      LItem.ImageKind := LImageKind;
      LItem.FullUrl := Format(FormattableString, [AImageSize.ToInteger]);
      LItem.FileName := UrlComponents.Strings[UrlComponents.Count - 4] + LImageKind.Extension;
      LItem.ImageSize := AImageSize;
      Result.Add(LItem);
    end;
  finally
    UrlComponents.Free;
  end;
end;

end.
