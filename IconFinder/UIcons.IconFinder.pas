unit UIcons.IconFinder;

interface

implementation

uses
  UIcons.Main, UIcons.Types, UIcons.BaseParser, UIcons.IconFinder.Parser, System.SysUtils, System.Classes,
  System.IOUtils, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP;

type
  TIFDownloader = class(TCustomIconDownloader)
  strict private
    procedure Download(const ADest: string; ParsedUrl: TParsedImage); reintroduce; overload;
  protected
    function BuildDownloadedItems(ParsedImages: TParsedImageList): TImageInfoList; override;
  public
    procedure Download(const ADest: string); overload; override;
  end;

{ TIFDownloader }

function TIFDownloader.BuildDownloadedItems(ParsedImages: TParsedImageList): TImageInfoList;
var
  LParsedImage: TParsedImage;
  LImageInfo: TImageInfo;
begin
  Result := TImageInfoList.Create;
  for LParsedImage in ParsedImages do
  begin
    LImageInfo.FileName := LParsedImage.FileName;
    LImageInfo.FileSize := LParsedImage.FileSize;
    LImageInfo.DownloadFolder := LParsedImage.DownloadFolder;
    LImageInfo.FilePath := LParsedImage.FilePath;
    LImageInfo.ImageSize := LParsedImage.ImageSize;
    LImageInfo.ImageKind := LParsedImage.ImageKind;
    LImageInfo.Overwrited := LParsedImage.Overwrited;
    Result.Add(LImageInfo);
  end;
end;

procedure TIFDownloader.Download(const ADest: string; ParsedUrl: TParsedImage);
var
  IdHTTP: TIdHTTP;
  AStream: TMemoryStream;
begin
  IdHTTP := TIdHTTP.Create(NIL);
  AStream := TMemoryStream.Create;
  try
    ParsedUrl.DownloadFolder := ADest;
    ParsedUrl.FilePath := TPath.Combine(ADest, ParsedUrl.ImageKind.ToDirString);
    ParsedUrl.FilePath := TPath.Combine(ParsedUrl.FilePath, ParsedUrl.ImageSize.ToString);

    if not(TDirectory.Exists(ParsedUrl.FilePath, False)) then
      if not(ForceDirectories(ParsedUrl.FilePath)) then
        raise Exception.CreateFmt('Could not create folder at %s', [ParsedUrl.FilePath]);

    ParsedUrl.FilePath := TPath.Combine(ParsedUrl.FilePath, ParsedUrl.FileName);
    if TFile.Exists(ParsedUrl.FilePath) then
      if not(Overwrite) then
        raise Exception.CreateFmt('%s already exists', [ParsedUrl.FilePath])
      else
        TFile.Delete(ParsedUrl.FilePath);

    IdHTTP.Get(ParsedUrl.FullUrl, AStream);
    ParsedUrl.FileSize := AStream.Size;
    AStream.Position := 0;
    AStream.SaveToFile(ParsedUrl.FilePath);
  finally
    AStream.Free;
    IdHTTP.Free;
  end;
end;

procedure TIFDownloader.Download(const ADest: string);
var
  Parser: IUrlParser;
  Item: TParsedImage;
  ImageList: TParsedImageList;
  DownloadedItems: TImageInfoList;
begin
  if DownloadMode = dmUnk then
    raise Exception.Create('Unspecified Download Mode');

  Parser := TIconFinderUrlParser.Create;
  ImageList := Parser.ParseUrl(Url);

  try
    if DownloadMode = dmAll then
      for Item in ImageList do
        Download(ADest, Item)
    else if DownloadMode = dmUrlOnly then
      Download(ADest, ImageList.First);

    if Assigned(OnCompleted) then
    begin
      DownloadedItems := BuildDownloadedItems(ImageList);
      OnCompleted(DownloadedItems);
      DownloadedItems.Free;
    end;
  finally
    ImageList.Free;
  end;
end;

var
  Interfaces: TInterfaceList;

initialization

Interfaces := TInterfaceList.Create;
Interfaces.Add(TIconFinderUrlParser.Create);
TIFDownloader.Register(TIFDownloader, Interfaces);

end.
