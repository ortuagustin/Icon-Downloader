unit UIcons.Types;

interface

uses
  // need System.SysUtils here because of compiler warning; so it can expand inline function calls
  System.SysUtils, System.Generics.Collections;

const
  EXT_PNG = '.png';
  EXT_ICO = '.ico';
  HTTP = 'http://';
  HTTPS = 'https://';

type
{$REGION 'Enum types'}
  TImageSize = (izUnk, iz16, iz24, iz32, iz48, iz64, iz72, iz96, iz128, iz256, iz512);
  TImageSizes = set of TImageSize;
  TImageKind = (ikUnk, ikPng, ikIco);
  TWebsite = (wsUnk, wsIconArchive, wsIconFinder);
  TDownloadMode = (dmUnk, dmUrlOnly, dmAll);
  TSizeUnit = (suByte, suKByte, suMByte, suGByte);
{$ENDREGION}

{$REGION 'Enum and custom helper types'}
  TByeConverter = class
  const
    BYTE = 1;
    KBYTE = 1024 * BYTE;
    MBYTE = 1024 * KBYTE;
    GBYTE = 1024 * MBYTE;
  private type
    TSizeUnitHelper = record helper for TSizeUnit
    public
      function ToString(const UseLongUnitNames: Boolean = False): string; inline;
    end;
  public
    class function ToString(const Value: Extended; const AUnit: TSizeUnit; Precision: Integer): string; inline;
    class function ToFmtString(const Value: Extended; const AUnit: TSizeUnit; const Precision: Integer): string;
  end;

  TImageSizeHelper = record helper for TImageSize
  public
    class function FromInteger(const Value: Integer): TImageSize; static; inline;
    class function ToInteger(const Value: TImageSize): Integer; overload; static; inline;
    function ToInteger: Integer; overload; inline;
    function ToString: string; inline;
  end;

  TImageSizesHelper = record helper for TImageSizes
  const
    Empty = [];
  public
    function IsEmpty: Boolean;
    function Count: Integer;
  end;

  TImageKindHelper = record helper for TImageKind
  public
    class function ToDirString(const Value: TImageKind): string; overload; static; inline;
    class function ToString(const Value: TImageKind): string; overload; static; inline;
    class function Extension(const Value: TImageKind): string; overload; static; inline;
    function ToDirString: string; overload; inline;
    function ToString: string; overload; inline;
    function Extension: string; overload; inline;
  end;
{$ENDREGION}

{$REGION 'Mock objects'}
  TImageInfo = record
  private
    FFileName: string;
    FSize: TImageSize;
    FKind: TImageKind;
    FDownloadFolder: string;
    FFilePath: string;
    FFileSize: Int64;
    FOverwrited: Boolean;
    procedure SetFileName(const Value: string);
    procedure SetKind(const Value: TImageKind);
    procedure SetSize(const Value: TImageSize);
    procedure SetDownloadFolder(const Value: string);
    procedure SetFilePath(const Value: string);
    procedure SeInt64(const Value: Int64);
    procedure SetOverwrited(const Value: Boolean);
  public
    function ToString(const AUnit: TSizeUnit; const Precision: Integer): string;
    property FileName: string read FFileName write SetFileName;
    property FileSize: Int64 read FFileSize write SeInt64;
    property DownloadFolder: string read FDownloadFolder write SetDownloadFolder;
    property FilePath: string read FFilePath write SetFilePath;
    property ImageSize: TImageSize read FSize write SetSize;
    property ImageKind: TImageKind read FKind write SetKind;
    property Overwrited: Boolean read FOverwrited write SetOverwrited;
  end;

  TImageInfoList = TList<TImageInfo>;
{$ENDREGION}

{$REGION 'Event types'}
  TOnCompletedEvent = procedure(DownloadedItems: TImageInfoList) of object;
{$ENDREGION}

implementation

uses
  System.Math;

{ TImageSizeHelper }

class function TImageSizeHelper.FromInteger(const Value: Integer): TImageSize;
begin
  case Value of
    16: Result := iz16;
    24: Result := iz24;
    32: Result := iz32;
    48: Result := iz48;
    64: Result := iz64;
    72: Result := iz72;
    96: Result := iz96;
    128: Result := iz128;
    256: Result := iz256;
    512: Result := iz512;
  else Result := izUnk;
  end;
end;

class function TImageSizeHelper.ToInteger(const Value: TImageSize): Integer;
begin
  case Value of
    izUnk: Result := -1;
    iz16: Result := 16;
    iz24: Result := 24;
    iz32: Result := 32;
    iz48: Result := 48;
    iz64: Result := 64;
    iz72: Result := 72;
    iz96: Result := 96;
    iz128: Result := 128;
    iz256: Result := 256;
    iz512: Result := 512;
    else Result := -1;
  end;
end;

function TImageSizeHelper.ToInteger: Integer;
begin
  Result := ToInteger(Self);
end;

function TImageSizeHelper.ToString: string;
begin
  Result := ToInteger(Self).ToString;
end;

{ TImageKindHelper }

class function TImageKindHelper.ToDirString(const Value: TImageKind): string;
begin
  case Value of
    ikUnk: Result := EmptyStr;
    ikPng: Result := 'Imagenes';
    ikIco: Result := 'Iconos';
  end;
end;

class function TImageKindHelper.Extension(const Value: TImageKind): string;
begin
  case Value of
    ikPng: Result := EXT_PNG;
    ikIco: Result := EXT_ICO;
  end;
end;

function TImageKindHelper.Extension: string;
begin
  Result := Extension(Self);
end;

function TImageKindHelper.ToDirString: string;
begin
  Result := ToDirString(Self);
end;

class function TImageKindHelper.ToString(const Value: TImageKind): string;
begin
  case Value of
    ikUnk: Result := 'Unknown';
    ikPng: Result := 'Png';
    ikIco: Result := 'Ico';
  end;
end;

function TImageKindHelper.ToString: string;
begin
  Result := ToString(Self);
end;

{ TImageSizesHelper }

function TImageSizesHelper.Count: Integer;
var
  Elem: TImageSize;
begin
  Result := 0;
  for Elem in Self do
    Inc(Result);
end;

function TImageSizesHelper.IsEmpty: Boolean;
begin
  Result := Self = Empty;
end;

{ TImgInfo }

procedure TImageInfo.SetFileName(const Value: string);
begin
  FFileName := Value;
end;

procedure TImageInfo.SetFilePath(const Value: string);
begin
  FFilePath := Value;
end;

procedure TImageInfo.SeInt64(const Value: Int64);
begin
  FFileSize := Value;
end;

procedure TImageInfo.SetDownloadFolder(const Value: string);
begin
  FDownloadFolder := Value;
end;

procedure TImageInfo.SetKind(const Value: TImageKind);
begin
  FKind := Value;
end;

procedure TImageInfo.SetOverwrited(const Value: Boolean);
begin
  FOverwrited := Value;
end;

procedure TImageInfo.SetSize(const Value: TImageSize);
begin
  FSize := Value;
end;

function TImageInfo.ToString(const AUnit: TSizeUnit; const Precision: Integer): string;
begin
  Result := 'FileName = ' + FileName + sLineBreak +
            'FileSize = ' + TByeConverter.ToFmtString(FileSize, AUnit, Precision) + sLineBreak +
            'DownloadFolder = ' + DownloadFolder + sLineBreak +
            'FilePath = ' + FilePath + sLineBreak +
            'ImageSize = ' + ImageSize.ToString + sLineBreak +
            'ImageKind = ' + ImageKind.ToString + sLineBreak +
            'Overwrited = ' + Overwrited.ToString;
end;

{ TByeConverter }

class function TByeConverter.ToFmtString(const Value: Extended; const AUnit: TSizeUnit; const Precision: Integer): string;
begin
  Result := ToString(Value, AUnit, Precision) + ' ' + AUnit.ToString;
end;

class function TByeConverter.ToString(const Value: Extended; const AUnit: TSizeUnit; Precision: Integer): string;
var
  Temp: Extended;
  FormatStr: string;
begin
  case AUnit of
    suByte: Temp := Value;
    suKByte: Temp := Value / KBYTE;
    suMByte: Temp := Value / MBYTE;
    suGByte: Temp := Value / GBYTE;
  end;

  if Precision = 0 then
    Precision := 1;

  FormatStr := '0.' + StringOfChar('#', Precision);

  Result := FormatFloat(FormatStr, Temp);
end;

{ TByeConverter.TSizeUnitHelper }

function TByeConverter.TSizeUnitHelper.ToString(const UseLongUnitNames: Boolean): string;
begin
  case Self of
    suByte: Result := 'Bytes';
    suKByte: Result := 'KBytes';
    suMByte: Result := 'Mbytes';
    suGByte: Result := 'GBytes';
  end;

  if not(UseLongUnitNames) and (Self <> suByte) then
    SetLength(Result, 2);
end;


end.
