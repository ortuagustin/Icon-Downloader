unit GUI.Main;

interface

uses
  UIcons.Types, System.Classes, System.SysUtils, Vcl.Controls, Vcl.Forms, Vcl.StdCtrls,
  Vcl.ExtCtrls;

type
  TFGUIMain = class(TForm)
    gpBottomToolBar: TGridPanel;
    edDestinationFolder: TEdit;
    BtnDownload: TButton;
    BtnChangeDestFolder: TButton;
    Label2: TLabel;
    pTopToolBar: TPanel;
    Label1: TLabel;
    cbURLS: TComboBox;
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure BtnChangeDestFolderClick(Sender: TObject);
    procedure BtnDownloadClick(Sender: TObject);
  private
    { Private declarations }
    procedure OnDownloadsCompleted(DownloadedItems: TImageInfoList);
  public
    { Public declarations }
  end;

var
  FGUIMain: TFGUIMain;

implementation

uses
  UIcons.Main, Vcl.StdActns;

{$R *.dfm}

procedure TFGUIMain.BtnChangeDestFolderClick(Sender: TObject);
begin
  with TBrowseForFolder.Create(NIL) do
  begin
    try
      Caption := 'Directorio destino donde descargar las imagenes..';
      RootDir := PathDelim;
      BrowseOptions := [bifEditBox, bifNewDialogStyle];

      if edDestinationFolder.Text <> EmptyStr then
        Folder := edDestinationFolder.Text;

      if (Execute) and (Folder <> EmptyStr) then
        edDestinationFolder.Text := Folder;
    finally
      Free;
    end;
  end;
end;

procedure TFGUIMain.FormCreate(Sender: TObject);
begin
  if ParamCount > 1 then
    edDestinationFolder.Text := ParamStr(1)
  else
    edDestinationFolder.Text := 'D:\Apps\__Recursos\';
end;

procedure TFGUIMain.OnDownloadsCompleted(DownloadedItems: TImageInfoList);
const
  DIVIDE = '------------------------------------';
var
  LItem: TImageInfo;
begin
  Memo1.Lines.Clear;
  for LItem in DownloadedItems do
  begin
    Memo1.Lines.Add(DIVIDE);
    Memo1.Lines.Add(LItem.ToString(suKByte, 2));
    Memo1.Lines.Add(DIVIDE);
  end;
end;

procedure TFGUIMain.BtnDownloadClick(Sender: TObject);
var
  Img: IImageDownload;
begin
  Img := TCustomIconDownloader.New(cbURLS.Text);
  Img.DownloadMode := dmAll;
  Img.OnCompleted := OnDownloadsCompleted;
  Img.Overwrite := True;
  Img.Download(edDestinationFolder.Text);
end;

end.
